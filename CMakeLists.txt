cmake_minimum_required(VERSION 2.8...3.19 FATAL_ERROR)

project(gen1int-molcaslib Fortran)

set(LIBDIR "" CACHE STRING "set path to directory for the installation of library files")
set(EXTRA_INCLUDE "" CACHE STRING "set path to directories to include")

set(CMAKE_MODULE_PATH
    ${CMAKE_MODULE_PATH}
    ${CMAKE_CURRENT_SOURCE_DIR}/cmake
    )

if(NOT CMAKE_Fortran_MODULE_DIRECTORY)
    set(CMAKE_Fortran_MODULE_DIRECTORY 
        ${PROJECT_BINARY_DIR}/modules)
endif()

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
include_directories(${CMAKE_Fortran_MODULE_DIRECTORY})
include_directories(${EXTRA_INCLUDE})

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE
        Release
        CACHE STRING
        "Choose the type of build, options are: None Debug Release."
        FORCE
        )
endif()

######################################################################
# Version information
######################################################################
set(GEN1INT_MOLCASLIB_YEAR 2016)
set(GEN1INT_MOLCASLIB_VERSION_MAJOR 1)
set(GEN1INT_MOLCASLIB_VERSION_MINOR 0)
set(GEN1INT_MOLCASLIB_VERSION_BUILD "")

if(EXISTS ${PROJECT_SOURCE_DIR}/.git)
    find_package(Git)
    if(GIT_FOUND)
        execute_process(
            COMMAND ${GIT_EXECUTABLE} rev-list --max-count=1 HEAD
            WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
            OUTPUT_VARIABLE GIT_REVISION
            ERROR_QUIET
            )
        if(NOT ${GIT_REVISION} STREQUAL "")
            string(STRIP ${GIT_REVISION} GIT_REVISION)
        endif()

        execute_process(
            COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD
            WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
            OUTPUT_VARIABLE GIT_BRANCH
            ERROR_QUIET
            )
        if(NOT ${GIT_BRANCH} STREQUAL "")
            string(STRIP ${GIT_BRANCH} GIT_BRANCH)
        endif()

    endif()
else(EXISTS ${PROJECT_SOURCE_DIR}/.git)
    set(GIT_REVISION "N/A")
    set(GIT_BRANCH   "N/A")
endif(EXISTS ${PROJECT_SOURCE_DIR}/.git)

set(GEN1INT_MOLCASLIB_VERSION_BUILD "${GIT_REVISION} (${GIT_BRANCH})")

set(GEN1INT_MOLCASLIB_VERSION "${GEN1INT_MOLCASLIB_VERSION_MAJOR}.${GEN1INT_MOLCASLIB_VERSION_MINOR}")
if(GEN1INT_MOLCASLIB_VERSION_BUILD)
  set(GEN1INT_MOLCASLIB_GIT_VERSION "${GEN1INT_MOLCASLIB_VERSION_BUILD}")
else(GEN1INT_MOLCASLIB_VERSION_BUILD)
  set(GEN1INT_MOLCASLIB_GIT_VERSION "")
endif(GEN1INT_MOLCASLIB_VERSION_BUILD)
set(GEN1INT_MOLCASLIB_VERSION_STRING "GEN1INT Molcas library - version: ${GEN1INT_MOLCASLIB_VERSION}")
MESSAGE(STATUS "GEN1INT Molcas library - version   :  ${GEN1INT_MOLCASLIB_VERSION}")
MESSAGE(STATUS "GEN1INT Molcas library - git commit:  ${GIT_REVISION} (branch: ${GIT_BRANCH})")

####################
# Set source files #
####################
set(SOURCES
${PROJECT_SOURCE_DIR}/src/aux_boys_vec.f90
${PROJECT_SOURCE_DIR}/src/contr_cgto_odist.f90        
${PROJECT_SOURCE_DIR}/src/gen1int_geom.f90            
${PROJECT_SOURCE_DIR}/src/london_ao.f90               
${PROJECT_SOURCE_DIR}/src/prim_hgto_nucpot.f90
${PROJECT_SOURCE_DIR}/src/binom_coeff.f90             
${PROJECT_SOURCE_DIR}/src/contr_sgto_carmom.f90       
${PROJECT_SOURCE_DIR}/src/gen1int_nucpot.f90          
${PROJECT_SOURCE_DIR}/src/london_mom_hgto.f90         
${PROJECT_SOURCE_DIR}/src/prim_hgto_odist.f90
${PROJECT_SOURCE_DIR}/src/carmom_deriv.f90            
${PROJECT_SOURCE_DIR}/src/contr_sgto_gaupot.f90       
${PROJECT_SOURCE_DIR}/src/gen1int_onehamil.f90        
${PROJECT_SOURCE_DIR}/src/main.f                      
${PROJECT_SOURCE_DIR}/src/reorder_ints.f90
${PROJECT_SOURCE_DIR}/src/carmom_hbra.f90             
${PROJECT_SOURCE_DIR}/src/contr_sgto_nucpot.f90       
${PROJECT_SOURCE_DIR}/src/geom_part_nucpot.f90        
${PROJECT_SOURCE_DIR}/src/next_permutation.f90        
${PROJECT_SOURCE_DIR}/src/shell_scatter.f90
${PROJECT_SOURCE_DIR}/src/carmom_hrr_ket.f90          
${PROJECT_SOURCE_DIR}/src/contr_sgto_odist.f90        
${PROJECT_SOURCE_DIR}/src/geom_part_zero.f90          
${PROJECT_SOURCE_DIR}/src/norm_contr_sgto.f90         
${PROJECT_SOURCE_DIR}/src/sort_cents.f90
${PROJECT_SOURCE_DIR}/src/carmom_moment.f90           
${PROJECT_SOURCE_DIR}/src/error_stop.f90              
${PROJECT_SOURCE_DIR}/src/geom_total.f90              
${PROJECT_SOURCE_DIR}/src/nucpot_geom.f90             
${PROJECT_SOURCE_DIR}/src/test_f90mod_sgto_angmom.f90
${PROJECT_SOURCE_DIR}/src/const_contr_ints.f90        
${PROJECT_SOURCE_DIR}/src/gaupot_geom.f90             
${PROJECT_SOURCE_DIR}/src/hgto_to_cgto.f90            
${PROJECT_SOURCE_DIR}/src/nucpot_hbra.f90             
${PROJECT_SOURCE_DIR}/src/test_f90mod_sgto_mag.f90
${PROJECT_SOURCE_DIR}/src/contr_cgto_carmom.f90       
${PROJECT_SOURCE_DIR}/src/gen1int.f90                 
${PROJECT_SOURCE_DIR}/src/hgto_to_sgto.f90            
${PROJECT_SOURCE_DIR}/src/nucpot_hket.f90             
${PROJECT_SOURCE_DIR}/src/xkind.f90
${PROJECT_SOURCE_DIR}/src/contr_cgto_gaupot.f90       
${PROJECT_SOURCE_DIR}/src/gen1int_carmom.f90          
${PROJECT_SOURCE_DIR}/src/html_log.f90                
${PROJECT_SOURCE_DIR}/src/prim_hgto_carmom.f90        
${PROJECT_SOURCE_DIR}/src/xtimer.f90
${PROJECT_SOURCE_DIR}/src/contr_cgto_nucpot.f90       
${PROJECT_SOURCE_DIR}/src/gen1int_gaupot.f90          
${PROJECT_SOURCE_DIR}/src/int_to_str.f90              
${PROJECT_SOURCE_DIR}/src/prim_hgto_gaupot.f90
    )

#################
# Build library #
#################
add_library(gen1int-molcaslib ${SOURCES})

###################
# Install library #
###################
if(NOT ${LIBDIR} STREQUAL "")
    install(TARGETS gen1int-molcaslib gen1int-molcaslib
            LIBRARY DESTINATION ${LIBDIR}/lib
            ARCHIVE DESTINATION ${LIBDIR}/lib)

else()
    install(TARGETS gen1int-molcaslib gen1int-molcaslib
            LIBRARY DESTINATION lib
            ARCHIVE DESTINATION lib)
endif()
